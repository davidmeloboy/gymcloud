package br.com.gymcloud.listener;

/**
 *
 * @author mateus_garbrecht
 */
import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class PhaseListenerAutenticacao implements PhaseListener {

    @Override
    public void afterPhase(PhaseEvent event) {
        try {
            FacesContext fc = event.getFacesContext();
            HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            HttpSession session = req.getSession();
            /*
            if (fc.getViewRoot().getViewId().lastIndexOf("index") == -1
                    && fc.getViewRoot().getViewId().lastIndexOf("timeout") == -1
                    && fc.getViewRoot().getViewId().lastIndexOf("login") == -1) {
                NavigationHandler nh = fc.getApplication().getNavigationHandler();
                nh.handleNavigation(fc, null, "/login.xhtml");
            } else {
                */
                if ((session.getAttribute("usuario") == null && fc.getViewRoot().getViewId().lastIndexOf("login") == -1)) {
                    if (fc.getViewRoot().getViewId().lastIndexOf("timeout") == -1) {
                        NavigationHandler nh = fc.getApplication().getNavigationHandler();
                        nh.handleNavigation(fc, null, "/login.xhtml");
                    }
                }
            
        } catch (Exception e) {
        } finally {
        }
    }

    @Override
    public PhaseId getPhaseId() {
        try {
        } catch (Exception e) {
        } finally {
            try {
                return PhaseId.RESTORE_VIEW;
            } catch (Exception e) {
                return null;
            }
        }
    }

    @Override
    public void beforePhase(PhaseEvent event) {
        try {
        } catch (Exception e) {
        }
    }
}
