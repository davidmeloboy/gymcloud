/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gymcloud.daos.impl;

import br.com.gymcloud.daos.GenericDao;
import java.io.Serializable;

/**
 *
 * @author David
 * @param <T>
 * @param <PK>
 */
public class GenericDaoImpl<T, PK extends Serializable> extends GenericDao<T,PK>{

    public GenericDaoImpl(Class objectClass) {
        super(objectClass);
    }
    
}
