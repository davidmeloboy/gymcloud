/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gymcloud.daos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;

public abstract class GenericDao<T, PK extends Serializable> implements TOBaseDao<T, PK> {

    

    private Session session;
    private Transaction tx;
    private final Class objectClass;
    private final List<Criterion> listCriterion;
    private final List<Order> listOrder;
    private final List<Projection> listProjection;

    public GenericDao(final Class objectClass) {
        this.objectClass = objectClass;
        this.listCriterion = new ArrayList();
        this.listOrder = new ArrayList();
        this.listProjection = new ArrayList();
    }

    @Override
    public Class getObjectClass() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T save(T object) {
        try {

            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            session.save(object);
            session.flush();
            tx.commit();

        } catch (final HibernateException e) {
            if (tx != null) {
                tx.rollback();
                return null;
            }
            System.out.println(e.getMessage());
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return object;
    }
    
    @Override
    public List<T> save(List<T> listObj) {
        try {

            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            
            for (T object : listObj) {
                session.save(object);
            }
            
            session.flush();
            tx.commit();

        } catch (final HibernateException e) {
            if (tx != null) {
                tx.rollback();
                return null;
            }
            System.out.println(e.getMessage());
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return listObj;
    }

    @Override
    public T save(T object, PK primaryKey) {
        try {
            if (exists(primaryKey)) {
                System.out.println("Registro já inserido anteriomente!");
                return null;
            } else {
                session = HibernateUtil.getSessionFactory().openSession();
            }

            tx = session.beginTransaction();
            session.save(object);
            tx.commit();

        } catch (final HibernateException ex) {
            if (tx != null) {
                tx.rollback();
                System.out.println(ex);
                return null;
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }

        }
        return object;
    }

    @Override
    public T load(PK primaryKey) {
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            final Object o = session.load(objectClass, primaryKey);
            return (T) o;
        } catch (final HibernateException ex) {
            System.out.println(ex);
            return null;
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public T get(PK primaryKey) {

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            final Object o = session.get(objectClass, primaryKey);
            return (T) o;
        } catch (final HibernateException ex) {
            System.out.println(ex);
            return null;
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public List listAll() {
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            final Criteria c = session.createCriteria(objectClass);
            addOrderToCriteria(c);
            return c.list();
        } catch (final HibernateException ex) {
            System.out.println(ex);
            return null;
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

    }

    @Override
    public List listAllQuery() {
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            final Criteria c = session.createCriteria(objectClass);
            Query queryProducts = session.createSQLQuery("select * from brcrs.acondicionamentos");
            List<Object[]> products = queryProducts.list();
            List<T> lista = new ArrayList<>();
            addOrderToCriteria(c);
            return c.list();
        } catch (final HibernateException ex) {
            System.out.println(ex);
            return null;
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public List list() {
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            final Criteria c = session.createCriteria(objectClass);
            for (Criterion cr : this.listCriterion) {
                c.add(cr);
            }
            for (Order o : this.listOrder) {
                c.addOrder(o);
            }
            for (Projection p : this.listProjection) {
                c.setProjection(p);
            }
            return c.list();

        } catch (final HibernateException ex) {
            System.out.println(ex);
            return null;
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public List listLazy() {
        try {
            final Criteria c = session.createCriteria(objectClass);
            for (Criterion cr : this.listCriterion) {
                c.add(cr);
            }
            for (Order o : this.listOrder) {
                c.addOrder(o);
            }
            for (Projection p : this.listProjection) {
                c.setProjection(p);
            }
            return c.list();
        } catch (final HibernateException ex) {
            System.out.println(ex);
            return null;
        }
    }

    @Override
    public List findByExample(T example) {
        try {

            this.session = HibernateUtil.getSessionFactory().openSession();
            final Criteria c = session.createCriteria(objectClass);
            c.add(Example.create(example).enableLike(MatchMode.START).ignoreCase().setPropertySelector((Example.PropertySelector) this));
            addOrderToCriteria(c);
            addPropertiedToCriteria(c, example);
            return c.list();
        } catch (final HibernateException ex) {
            System.out.println(ex);
            return null;
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public T findOneByExample(T example) {
        final List res = findByExample(example, 0, 1);
        if ((res != null) && (res.size() == 1)) {
            return (T) res.get(0);
        } else {
            return null;
        }
    }

    @Override
    public List listAll(int first, int max) {
        try {
            final Criteria c = session.createCriteria(objectClass);
            addOrderToCriteria(c);
            if (first != 0) {
                c.setFirstResult(first);
            }
            if (max != 0) {
                c.setMaxResults(max);
            }
            return c.list();
        } catch (final HibernateException ex) {
            System.out.println(ex);
            return null;
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public int listAllPageCount() {
        final List l = listAll();
        final Integer i = l.size();
        return i;
    }

    @Override
    public List findByExample(T example, int first, int max) {
        try {
            this.session = HibernateUtil.getSessionFactory().openSession();
            final Criteria c = session.createCriteria(objectClass);
            c.add(Example.create(example).enableLike(MatchMode.ANYWHERE).ignoreCase().setPropertySelector((Example.PropertySelector) this));
            addPropertiedToCriteria(c, example);
            addOrderToCriteria(c);
            if (first != 0) {
                c.setFirstResult(first);
            }
            if (max != 0) {
                c.setMaxResults(max);
            }
            return c.list();
        } catch (final HibernateException ex) {
            System.out.println(ex);
            return null;
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public int findByExamplePageCount(T example) {
        final List l = findByExample(example);
        final Integer i = l.size();
        return i;
    }

    @Override
    public void update(T object) {
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            session.update(object);
            tx.commit();
        } catch (final HibernateException ex) {
            System.out.println(ex);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public void delete(T object) {
        try {
            if (object != null) {
                session = HibernateUtil.getSessionFactory().openSession();
                tx = session.beginTransaction();
                session.delete(object);
                tx.commit();
            }
        } catch (final HibernateException ex) {
            System.out.println(ex);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public void refresh(T object) {
        session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.refresh(object);
        } catch (final HibernateException ex) {
            System.out.println(ex);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public boolean exists(PK primaryKey) {
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            final Object o = session.get(objectClass, primaryKey);
            return o != null;
        } catch (final HibernateException ex) {
            System.out.println(ex);
            return false;
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

    }

    protected void addPropertiedToCriteria(final Criteria c, final T example) {
    }

    protected void addOrderToCriteria(Criteria c) {
    }

    public void addRestriction(Criterion c) {
        this.listCriterion.add(c);
    }

    public void addOrder(Order o) {
        this.listOrder.add(o);
    }

    public void addProjection(Projection p) {
        this.listProjection.add(p);
    }

}
