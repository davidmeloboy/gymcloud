/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gymcloud.testes;

import br.com.gymcloud.daos.impl.GenericDaoImpl;
import br.com.gymcloud.entities.ClassificacaoImc;
import br.com.gymcloud.entities.GruposMusculares;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author David
 */
public class Teste2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        GenericDaoImpl d = new GenericDaoImpl(ClassificacaoImc.class);
        List<ClassificacaoImc> gm = d.list();
        
        for(ClassificacaoImc c : gm){
             System.out.println(c.getClassificacao());
        }
        
        
    }
    
}
