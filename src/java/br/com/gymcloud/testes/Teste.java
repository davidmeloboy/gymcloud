/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gymcloud.testes;

import br.com.gymcloud.daos.HibernateUtil;
import br.com.gymcloud.entities.GruposMusculares;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author David
 */
public class Teste {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Session session = null;
        Transaction tx = null;

        try {
      // aqui nós lemos as configurações do arquivo hibernate.cfg.xml
            // e deixamos o Hibernate pronto para trabalhar
           // SessionFactory factory = new Configuration().configure().buildSessionFactory();
            session = HibernateUtil.getSessionFactory().openSession();

            // abre uma nova sessão
            //session = factory.openSession();

            // inicia uma transação
            tx = session.beginTransaction();

      // vamos criar uma nova instância da classe 
            // e definir valores para seus atributos
            // note que não precisamos atribuir valores para
            // o atributo id
            GruposMusculares g = new GruposMusculares();
            g.setGmNome("TESTE 6");

            session.save(g); // vamos salvar o usuário
            session.flush();

            // e salva as alterações no banco de dados
            tx.commit();
        } catch (Exception e) {
      // houve algum problema? vamos retornar o banco de dados
            // ao seu estado anterior
            if (tx != null) {
                tx.rollback();
            }
            System.out.println(e.getMessage());
        } finally {
          //  session.close();
        }
    }
}
