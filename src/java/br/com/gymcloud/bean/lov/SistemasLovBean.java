package br.com.gymcloud.bean.lov;

import br.com.gymcloud.daos.impl.GenericDaoImpl;
import br.com.gymcloud.entities.Sistemas;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author mateus_garbrecht
 */
@ManagedBean(name = "sistemasLovBean")
@ViewScoped
public class SistemasLovBean extends BaseLov implements Serializable {

    private String sisCodigo;
    private String sisDescricao;
    private List<Sistemas> listaSistemas;

    public String getSisCodigo() {
        return sisCodigo;
    }

    public void setSisCodigo(String sisCodigo) {
        this.sisCodigo = sisCodigo;
    }

    public String getSisDescricao() {
        return sisDescricao;
    }

    public void setSisDescricao(String sisDescricao) {
        this.sisDescricao = sisDescricao;
    }

    public List<Sistemas> getListaSistemas() {
        return listaSistemas;
    }

    public void setListaEmpresas(List<Sistemas> listaSistemas) {
        this.listaSistemas = listaSistemas;
    }

    @Override
    public void select() {
        GenericDaoImpl sistemasDao = new GenericDaoImpl(Sistemas.class);
        if (!this.sisCodigo.isEmpty()) {
            sistemasDao.addRestriction(Restrictions.like("sisCodigo", new BigDecimal(this.sisCodigo)));
        }
        if (!this.sisDescricao.isEmpty()) {
            sistemasDao.addRestriction(Restrictions.like("sistemasDao", this.sisDescricao.toUpperCase()));
        }
        this.listaSistemas = sistemasDao.list();
    }

    @Override
    public void reset() {
        this.sisCodigo = null;
        this.sisDescricao = null;
        this.listaSistemas = null;
    }

    @Override
    public void removeFoco() {
        super.removeFoco();
        reset();
    }
}
