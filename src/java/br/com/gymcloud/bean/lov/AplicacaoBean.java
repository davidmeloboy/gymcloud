package br.com.gymcloud.bean.lov;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author mateus_garbrecht
 */
@ManagedBean(name = "aplicacaoBean")
@SessionScoped
public class AplicacaoBean implements Serializable {

    private String lov;

    public String getLov() {
        return lov;
    }

    public void setLov(String lov) {
        this.lov = lov;
    }

    public AplicacaoBean() {
        this.lov = "";
    }

    public void voidAction() {
    }
}
