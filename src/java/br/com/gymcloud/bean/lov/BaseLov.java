package br.com.gymcloud.bean.lov;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;

/**
 *
 * @author mateus_garbrecht
 */
@ManagedBean(name = "baseLovBean")
@SessionScoped
public class BaseLov  {

    private String lov;
    private String parametro1;
    private String parametro2;
    private String parametro3;
    private String parametro4;
    private String parametro5;
    private String retorno1;
    private String retorno2;
    private String retorno3;
    private String retorno4;
    private String retorno5;
    private boolean executeQuery;
    private boolean rendered;
    private boolean resetFull;
    @ManagedProperty(value = "#{aplicacaoBean}")
    private AplicacaoBean aplicacaoBean;    

    
    public BaseLov() {
        this.rendered = false;
    }

    public AplicacaoBean getAplicacaoBean() {
        return aplicacaoBean;
    }

    public void setAplicacaoBean(AplicacaoBean aplicacaoBean) {
        this.aplicacaoBean = aplicacaoBean;
    }

    public boolean isRendered() {
        return rendered;
    }

    public void setRendered(boolean rendered) {
        this.rendered = rendered;
    }

    public boolean isExecuteQuery() {
        return executeQuery;
    }

    public void setExecuteQuery(boolean executeQuery) {
        this.executeQuery = executeQuery;
    }

    public boolean isResetFull() {
        return resetFull;
    }

    public void setResetFull(boolean resetFull) {
        this.resetFull = resetFull;
    }

    public String getLov() {
        return lov;
    }

    public void setLov(String lov) {
        this.rendered = true;
        this.lov = lov;
    }

    public String getParametro1() {
        return parametro1;
    }

    public void setParametro1(String parametro1) {
        this.parametro1 = parametro1;
    }

    public String getParametro2() {
        return parametro2;
    }

    public void setParametro2(String parametro2) {
        this.parametro2 = parametro2;
    }

    public String getParametro3() {
        return parametro3;
    }

    public void setParametro3(String parametro3) {
        this.parametro3 = parametro3;
    }

    public String getParametro4() {
        return parametro4;
    }

    public void setParametro4(String parametro4) {
        this.parametro4 = parametro4;
    }

    public String getParametro5() {
        return parametro5;
    }

    public void setParametro5(String parametro5) {
        this.parametro5 = parametro5;
    }

    public String getRetorno1() {
        return retorno1;
    }

    public void setRetorno1(String retorno1) {
        this.retorno1 = retorno1;
    }

    public String getRetorno2() {
        return retorno2;
    }

    public void setRetorno2(String retorno2) {
        this.retorno2 = retorno2;
    }

    public String getRetorno3() {
        return retorno3;
    }

    public void setRetorno3(String retorno3) {
        this.retorno3 = retorno3;
    }

    public String getRetorno4() {
        return retorno4;
    }

    public void setRetorno4(String retorno4) {
        this.retorno4 = retorno4;
    }

    public String getRetorno5() {
        return retorno5;
    }

    public void setRetorno5(String retorno5) {
        this.retorno5 = retorno5;
    }

    public void setaParametros(ActionEvent event) {
        
        this.executeQuery = event.getComponent().getAttributes().get("executeQuery") != null;
        this.resetFull = event.getComponent().getAttributes().get("resetFull") != null;

        if (event.getComponent().getAttributes().get("parametro1") != null) {
            this.parametro1 = (String) (event.getComponent().getAttributes().get("parametro1") + "");
        }
        if (event.getComponent().getAttributes().get("parametro2") != null) {
            this.parametro2 = (String) (event.getComponent().getAttributes().get("parametro2") + "");
        }
        if (event.getComponent().getAttributes().get("parametro3") != null) {
            this.parametro3 = (String) (event.getComponent().getAttributes().get("parametro3") + "");
        }
        if (event.getComponent().getAttributes().get("parametro4") != null) {
            this.parametro4 = (String) (event.getComponent().getAttributes().get("parametro4") + "");
        }
        if (event.getComponent().getAttributes().get("parametro5") != null) {
            this.parametro5 = (String) (event.getComponent().getAttributes().get("parametro5") + "");
        }
        if (event.getComponent().getAttributes().get("lov") != null) {
            this.lov = "/lov/" + (String) (event.getComponent().getAttributes().get("lov") + ".xhtml");
        }
        aplicacaoBean.setLov(this.lov);
        if (event.getComponent().getAttributes().get("retorno1") != null) {
            this.retorno1 = "#" + ((event.getComponent().getClientId()).replace(event.getComponent().getId(), (String) event.getComponent().getAttributes().get("retorno1"))).replace(":", "\\\\:");
        }
        if (event.getComponent().getAttributes().get("retorno2") != null) {
            this.retorno2 = "#" + ((event.getComponent().getClientId()).replace(event.getComponent().getId(), (String) event.getComponent().getAttributes().get("retorno2"))).replace(":", "\\\\:");
        }
        if (event.getComponent().getAttributes().get("retorno3") != null) {
            this.retorno3 = "#" + ((event.getComponent().getClientId()).replace(event.getComponent().getId(), (String) event.getComponent().getAttributes().get("retorno3"))).replace(":", "\\\\:");
        }
        if (event.getComponent().getAttributes().get("retorno4") != null) {
            this.retorno4 = "#" + ((event.getComponent().getClientId()).replace(event.getComponent().getId(), (String) event.getComponent().getAttributes().get("retorno4"))).replace(":", "\\\\:");
        }
        if (event.getComponent().getAttributes().get("retorno5") != null) {
            this.retorno5 = "#" + ((event.getComponent().getClientId()).replace(event.getComponent().getId(), (String) event.getComponent().getAttributes().get("retorno5"))).replace(":", "\\\\:");
        }
        if (this.executeQuery) {
            this.select();
        }
        if (this.resetFull) {
            this.reset();
        }
        this.rendered = true;
    }

    public void select() {
    }

    public void reset() {
    }

    public void removeFoco() {
    }
}
