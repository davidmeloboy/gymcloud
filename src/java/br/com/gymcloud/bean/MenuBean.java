package br.com.gymcloud.bean;

import br.com.gymcloud.daos.impl.GenericDaoImpl;
import br.com.gymcloud.entities.Modulos;
import br.com.gymcloud.entities.Sistemas;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.menu.DefaultSubMenu;

/**
 *
 * @author mateus_garbrecht
 */
@ManagedBean
@ViewScoped
public class MenuBean implements Serializable {

    private MenuModel menuModel;
    private String paginaAtual;

    @PostConstruct
    public void init() {
        menuModel = new DefaultMenuModel();
        GenericDaoImpl sistemasDao = new GenericDaoImpl(Sistemas.class);
        GenericDaoImpl modulosDao;
        for (Sistemas sistema : (List<Sistemas>) sistemasDao.list()) {

            DefaultSubMenu menu = new DefaultSubMenu(sistema.getSisDescricao());
            modulosDao = new GenericDaoImpl(Modulos.class);
            modulosDao.addRestriction(Restrictions.eq("sistemas", sistema));
            for (Modulos modulo : (List<Modulos>) modulosDao.list()) {
                DefaultMenuItem item = new DefaultMenuItem(modulo.getModDescricao());
                item.setIcon("ui-icon-document");
                item.setCommand("#{menuBean.setPaginaAtual('" + modulo.getModCaminho() + "')}");
                item.setAjax(true);
                item.setUpdate(":centerContentPanel");
                item.setOnstart("PF('statusDialog').show()");
                item.setOncomplete("PF('statusDialog').hide()");
                menu.addElement(item);
                
            }
            menuModel.addElement(menu);
        }
        //menuModel.addElement(menu);
    }

    public MenuModel getMenu() {
        return menuModel;
    }

    public MenuModel getMenuModel() {
        return menuModel;
    }

    public void setMenuModel(MenuModel menuModel) {
        this.menuModel = menuModel;
    }

    public String getPaginaAtual() {
        return paginaAtual;
    }

    public void setPaginaAtual(String paginaAtual) {
        System.out.println("Setou pagina atual: " + paginaAtual);
        this.paginaAtual = paginaAtual;
    }

}
