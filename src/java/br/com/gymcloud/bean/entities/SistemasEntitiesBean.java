package br.com.gymcloud.bean.entities;

import br.com.gymcloud.daos.impl.GenericDaoImpl;
import br.com.gymcloud.entities.Sistemas;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author mateus_garbrecht
 */
@ManagedBean(name = "sistemasEntitiesBean")
@ViewScoped
public class SistemasEntitiesBean implements Serializable {

    public String getSisDescricao(String sisCodigo) {
        String sisDescricao = null;
        GenericDaoImpl sistemasDao = new GenericDaoImpl(Sistemas.class);
        Sistemas sistema;
        try {
            if (!sisCodigo.isEmpty()) {
                sistema = (Sistemas) sistemasDao.get(new BigDecimal(sisCodigo));
                sisDescricao = sistema.getSisDescricao().toUpperCase();
            }
        } catch (Exception e) {
        }
        return sisDescricao;
    }
}
