package br.com.gymcloud.bean;

import br.com.gymcloud.daos.impl.GenericDaoImpl;
import br.com.gymcloud.entities.Pessoas;
import br.com.gymcloud.util.Mensagem;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author mateus_garbrecht
 */
@ManagedBean
@RequestScoped
public class LoginBean {

    private String usuario;
    private String senha;

    public LoginBean() {
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public void login() {
        GenericDaoImpl pessoasDao = new GenericDaoImpl(Pessoas.class);
        try {
            pessoasDao.addRestriction(Restrictions.eq("psCodigo", new BigDecimal(this.usuario)));
            pessoasDao.addRestriction(Restrictions.eq("psSenha", this.senha));
            List<Pessoas> listaPessoas = pessoasDao.list();
            if (listaPessoas != null && listaPessoas.size() > 0) {
                HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
                HttpSession session = req.getSession();
                session.setAttribute("usuario", listaPessoas.get(0));
                FacesContext.getCurrentInstance().getExternalContext().redirect("/gymcloud");
            } else {
                Mensagem mensagem = new Mensagem();
                mensagem.adicionarMensagem("E", "Usuário ou senha inválidos!");
            }

        } catch (Exception e) {
            Mensagem mensagem = new Mensagem();
            mensagem.adicionarMensagem("E", "Usuário ou senha inválidos!");
        }
    }

    public void logout() {
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpSession session = req.getSession();
        try {
            session.invalidate();
            FacesContext.getCurrentInstance().getExternalContext().redirect("/gymcloud");
        } catch (IOException ex) {
            Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void reset() {
        this.usuario = null;
        this.senha = null;
    }

}
