package br.com.gymcloud.bean.administrativo;

import br.com.gymcloud.daos.impl.GenericDaoImpl;
import br.com.gymcloud.entities.Modulos;
import br.com.gymcloud.util.Mensagem;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author mateus_garbrecht
 */
@ManagedBean(name = "modulosBean")
@ViewScoped
public class ModulosBean {

    private BigDecimal modCodigo;
    private BigDecimal sisCodigo;
    private String modCaminho;
    private String modDescricao;
    private Modulos selectedModulo;
    private String operacao;
    private List<Modulos> listaModulos;

    
    public ModulosBean() {
        this.listaModulos = new ArrayList();
        this.selectedModulo = new Modulos();
    }

    public BigDecimal getModCodigo() {
        return modCodigo;
    }

    public void setModCodigo(BigDecimal modCodigo) {
        this.modCodigo = modCodigo;
    }

    public BigDecimal getSisCodigo() {
        return sisCodigo;
    }

    public void setSisCodigo(BigDecimal sisCodigo) {
        this.sisCodigo = sisCodigo;
    }

    public String getModCaminho() {
        return modCaminho;
    }

    public void setModCaminho(String modCaminho) {
        this.modCaminho = modCaminho;
    }

    public String getModDescricao() {
        return modDescricao;
    }

    public void setModDescricao(String modDescricao) {
        this.modDescricao = modDescricao;
    }

    public Modulos getSelectedModulo() {
        return selectedModulo;
    }

    public void setSelectedModulo(Modulos selectedModulo) {
        this.selectedModulo = selectedModulo;
    }

    public void setSelectedMenu(Modulos selectedModulo) {
        this.selectedModulo = selectedModulo;
    }

    public String getOperacao() {
        return operacao;
    }

    public void setOperacao(String operacao) {
        this.selectedModulo = new Modulos();
        this.operacao = operacao;
    }

    public List<Modulos> getListaModulos() {
        return listaModulos;
    }

    public void setListaModulos(List<Modulos> listaModulos) {
        this.listaModulos = listaModulos;
    }

    public void reset() {
        this.modCodigo = null;
        this.sisCodigo = null;
        this.modCaminho = null;
        this.modDescricao = null;
        this.selectedModulo = new Modulos();
        this.listaModulos = new ArrayList();
    }

    public void select() {
        GenericDaoImpl modulosDao = new GenericDaoImpl(Modulos.class);
        if (this.modCodigo != null) {
            modulosDao.addRestriction(Restrictions.eq("modCodigo", this.modCodigo));
        }
        /*
         if (this.sisCodigo != null) {
         modulosDao.addRestriction(Restrictions.like("sisCodigo", this.sisCodigo));
         }
         */
        if (!this.modCaminho.isEmpty()) {
            modulosDao.addRestriction(Restrictions.like("modCaminho", this.modCaminho).ignoreCase());
        }
        if (!this.modDescricao.isEmpty()) {
            modulosDao.addRestriction(Restrictions.eq("modDescricao", this.modDescricao));
        }
        modulosDao.addOrder(Order.asc("modCodigo"));
        this.listaModulos = modulosDao.list();
    }

    public void insert() {
        Mensagem mensagem = new Mensagem();
        GenericDaoImpl modulosDao = new GenericDaoImpl(Modulos.class);
        modulosDao.save(this.selectedModulo);
        this.listaModulos.add(this.selectedModulo);
        this.selectedModulo = new Modulos();
        setOperacao("I");
        mensagem.adicionarMensagem("I", "Operação realizada com sucesso!");
    }

    public void update() {
        Mensagem mensagem = new Mensagem();
        GenericDaoImpl modulosDao = new GenericDaoImpl(Modulos.class);
        modulosDao.update(this.selectedModulo);
        this.selectedModulo = new Modulos();
        mensagem.adicionarMensagem("I", "Operação realizada com sucesso!");
    }

    public void delete() {
        Mensagem mensagem = new Mensagem();
        GenericDaoImpl modulosDao = new GenericDaoImpl(Modulos.class);
        modulosDao.delete(this.selectedModulo);
        this.listaModulos.remove(this.selectedModulo);
        this.selectedModulo = new Modulos();
        mensagem.adicionarMensagem("I", "Operação realizada com sucesso!");
    }

}
