package br.com.gymcloud.util;

/**
 *
 * @author mateus_garbrecht
 */

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public class Mensagem {

    public void adicionarMensagem(String severity, String mensagem) {
        FacesMessage fm = new FacesMessage();
        if (severity.equalsIgnoreCase("E")) {
            fm.setSeverity(FacesMessage.SEVERITY_ERROR);
        } else if (severity.equalsIgnoreCase("F")) {
            fm.setSeverity(FacesMessage.SEVERITY_FATAL);
        } else if (severity.equalsIgnoreCase("I")) {
            fm.setSeverity(FacesMessage.SEVERITY_INFO);
        } else if (severity.equalsIgnoreCase("W")) {
            fm.setSeverity(FacesMessage.SEVERITY_WARN);
        } else {
            fm.setSeverity(FacesMessage.SEVERITY_WARN);
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(fm.getSeverity(), mensagem, ""));
    }

    public void adicionarMensagem(String idMessage, String severity, String mensagem) {
        FacesMessage fm = new FacesMessage();
        if (severity.equalsIgnoreCase("E")) {
            fm.setSeverity(FacesMessage.SEVERITY_ERROR);
        } else if (severity.equalsIgnoreCase("F")) {
            fm.setSeverity(FacesMessage.SEVERITY_FATAL);
        } else if (severity.equalsIgnoreCase("I")) {
            fm.setSeverity(FacesMessage.SEVERITY_INFO);
        } else {
            fm.setSeverity(FacesMessage.SEVERITY_WARN);
        }
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(idMessage, new FacesMessage(fm.getSeverity(), mensagem, ""));
    }

    public void removerMensagens() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.getMessages().remove();
    }
}

