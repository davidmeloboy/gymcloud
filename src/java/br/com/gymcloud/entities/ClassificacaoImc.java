/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gymcloud.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author David
 */
@Entity
@Table(name = "CLASSIFICACAO_IMC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClassificacaoImc.findAll", query = "SELECT c FROM ClassificacaoImc c"),
    @NamedQuery(name = "ClassificacaoImc.findByImcCodigo", query = "SELECT c FROM ClassificacaoImc c WHERE c.imcCodigo = :imcCodigo"),
    @NamedQuery(name = "ClassificacaoImc.findByImcInicial", query = "SELECT c FROM ClassificacaoImc c WHERE c.imcInicial = :imcInicial"),
    @NamedQuery(name = "ClassificacaoImc.findByImcFinal", query = "SELECT c FROM ClassificacaoImc c WHERE c.imcFinal = :imcFinal"),
    @NamedQuery(name = "ClassificacaoImc.findByClassificacao", query = "SELECT c FROM ClassificacaoImc c WHERE c.classificacao = :classificacao")})
public class ClassificacaoImc implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @SequenceGenerator(sequenceName = "SEQ_CLASSIFICACAO_IMC",name = "classificacao_imc_generator",initialValue = 1,allocationSize = 1,schema = "GYMCLOUD")
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "classificacao_imc_generator")
    @Column(name = "IMC_CODIGO")
    private BigDecimal imcCodigo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IMC_INICIAL")
    private BigDecimal imcInicial;
    @Column(name = "IMC_FINAL")
    private BigDecimal imcFinal;
    @Size(max = 100)
    @Column(name = "CLASSIFICACAO")
    private String classificacao;

    public ClassificacaoImc() {
    }

    public ClassificacaoImc(BigDecimal imcCodigo) {
        this.imcCodigo = imcCodigo;
    }

    public ClassificacaoImc(BigDecimal imcCodigo, BigDecimal imcInicial) {
        this.imcCodigo = imcCodigo;
        this.imcInicial = imcInicial;
    }

    public BigDecimal getImcCodigo() {
        return imcCodigo;
    }

    public void setImcCodigo(BigDecimal imcCodigo) {
        this.imcCodigo = imcCodigo;
    }

    public BigDecimal getImcInicial() {
        return imcInicial;
    }

    public void setImcInicial(BigDecimal imcInicial) {
        this.imcInicial = imcInicial;
    }

    public BigDecimal getImcFinal() {
        return imcFinal;
    }

    public void setImcFinal(BigDecimal imcFinal) {
        this.imcFinal = imcFinal;
    }

    public String getClassificacao() {
        return classificacao;
    }

    public void setClassificacao(String classificacao) {
        this.classificacao = classificacao;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (imcCodigo != null ? imcCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClassificacaoImc)) {
            return false;
        }
        ClassificacaoImc other = (ClassificacaoImc) object;
        if ((this.imcCodigo == null && other.imcCodigo != null) || (this.imcCodigo != null && !this.imcCodigo.equals(other.imcCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.gymcloud.entities.ClassificacaoImc[ imcCodigo=" + imcCodigo + " ]";
    }
    
}
